package the;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import the.F.v001;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }
    public static void main(String... args) {
        //launch(args);
        v001 _001 = new v001();
        _001.start();

        //IWalker walker = new FileCppHWalker(Paths.get("d:/Projects/Perfect/"));
        //walker.walk();
    }
}
