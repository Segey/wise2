package the.walker.interfaces;

/**
 * Created by SPanin on 7/14/2016.
 */
public interface IWalker {
    public void walk(IWalkerHandler handler);
}

