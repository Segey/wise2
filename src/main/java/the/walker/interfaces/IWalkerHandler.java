package the.walker.interfaces;

import java.util.List;

/**
 * Created by SPanin on 7/15/2016.
 */
public interface IWalkerHandler {
    public void data(List<String> str);
}
