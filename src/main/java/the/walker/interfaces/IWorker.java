package the.walker.interfaces;

/**
 * Created by SPanin on 7/15/2016.
 */
public interface IWorker {
    public void work();
}
