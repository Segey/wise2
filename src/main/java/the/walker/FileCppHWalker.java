package the.walker;
import the.walker.Visitors.FileCppHVisitor;
import the.walker.interfaces.IWalker;
import the.walker.interfaces.IWalkerHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by SPanin on 7/14/2016.
 */
public class FileCppHWalker extends BaseWalker implements IWalker {
    public FileCppHWalker(Path path) {
        super(path);
    }
    @Override
    public void walk(IWalkerHandler handler) {
        try {
            Files.walkFileTree(super.path(), new FileCppHVisitor(handler));
        } catch (IOException e) {
            System.out.println(String.format("- An Error occurs(%s)",e));
        }
    }
}
