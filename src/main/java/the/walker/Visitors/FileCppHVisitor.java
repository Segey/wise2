package the.walker.Visitors;
import the.walker.interfaces.IWalkerHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by SPanin on 7/14/2016.
 */
public class FileCppHVisitor implements FileVisitor<Path> {
    private static PathMatcher m_file_matcher = null;
    private static PathMatcher m_dir_matcher = null;
    private IWalkerHandler m_handler = null;

    static {
        m_dir_matcher = FileSystems.getDefault().getPathMatcher("glob:*.git");
        m_file_matcher = FileSystems.getDefault().getPathMatcher("regex:[^(moc_|qrc)].*\\.(cpp|h)");
    }

    public FileCppHVisitor(IWalkerHandler handler) {
        m_handler = handler;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return m_dir_matcher.matches(dir.getFileName())
                ? FileVisitResult.SKIP_SUBTREE
                : FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(!m_file_matcher.matches(file.getFileName())) return FileVisitResult.CONTINUE;

        m_handler.data(Files.readAllLines(file, StandardCharsets.UTF_8));
        return FileVisitResult.TERMINATE;
    }
    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
}
