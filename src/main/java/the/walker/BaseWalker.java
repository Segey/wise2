package the.walker;
import java.nio.file.Path;

/**
 * Created by SPanin on 7/14/2016.
 */
public class BaseWalker {
    private Path m_path = null;

    public BaseWalker(Path path) {
        setPath(path);
    }
    Path path() {
        return m_path;
    }
    void setPath(Path path) {
        m_path = path;
    }
}
