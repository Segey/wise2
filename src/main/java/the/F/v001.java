package the.F;

import the.walker.FileCppHWalker;
import the.walker.interfaces.IWalker;
import the.walker.interfaces.IWalkerHandler;
import the.walker.interfaces.IWorker;

import java.nio.file.Paths;
import java.util.List;

/**
 * Created by SPanin on 7/15/2016.
 */
public class v001 extends Thread {
    class Worker implements IWorker {
        public void work() {

            System.out.println("Works v001 - done");
        }
    }
    class WalkerHandler implements IWalkerHandler {
        public void data(List<String> list) {
            for(String s: list)
                System.out.println(s);
        }
    }

    public void run() {
        IWalker walker = new FileCppHWalker(Paths.get("d:/Projects/Perfect/"));
        walker.walk(new WalkerHandler());
    }
}
